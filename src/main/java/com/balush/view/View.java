package com.balush.view;

public class View {
    public void showUserFunction() {
        System.out.println("1 - show passenger capacity" + '\n'
                + "2 - show load capacity" + '\n'
                + "3 - sort by range of flight" + '\n'
                + "4 - show in range of fuel consumption" + '\n'
                + "Default - Back:");
    }

    public void showAdminFunction() {
        System.out.println("1 - show passenger capacity" + '\n'
                + "2 - show load capacity" + '\n'
                + "3 - sort by range of flight" + '\n'
                + "4 - show in range of fuel consumption" + '\n'
                + "5 - add new airplane" + '\n'
                + "6 - change status of flight" + '\n'
                + "7 - show all flights" + '\n'
                + "8 - show all airplanes" + '\n'
                + "9 - add new flight" + '\n'
                + "Default - Back:");
    }

    public void showStartMenu() {
        System.out.println("Welcome");
        System.out.println("Choose your view mode:" + '\n'
                + "1 - Airline admin" + '\n'
                + "2 - User" + '\n'
                + "Default - Exit:");
    }
}
