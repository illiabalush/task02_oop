package com.balush.model.system;

import com.balush.model.object.Airplane;

public class Flight {
    private static int flightCounter;
    private final int flightID;
    private Airplane airplane;
    private String cityFrom;
    private String cityTo;
    private String departure;
    private String arrival;
    private boolean status;

    public Flight(final Airplane airplane, final String cityFrom,
                  final String cityTo, final String departure, final String arrival) {
        this.airplane = airplane;
        this.cityFrom = cityFrom;
        this.cityTo = cityTo;
        this.departure = departure;
        this.arrival = arrival;
        this.status = false;
        flightID = ++flightCounter;
    }

    public int getFlightID() {
        return flightID;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public String getDeparture() {
        return departure;
    }

    public String getArrival() {
        return arrival;
    }

    public boolean getStatus() {
        return status;
    }

    public void setAirplane(final Airplane airplane) {
        this.airplane = airplane;
    }

    public void setCityFrom(final String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public void setCityTo(final String cityTo) {
        this.cityTo = cityTo;
    }

    public void setDeparture(final String departure) {
        this.departure = departure;
    }

    public void setArrival(final String arrival) {
        this.arrival = arrival;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public void showFlightInfo() {
        System.out.printf("%5d\t|%13s\t\t|%13s\t\t|%13s\t\t|"
                        + "%10s\t\t|%10s\t\t|%10b\n", flightID, airplane.getModel(),
                cityFrom, cityTo, departure, arrival, status);
    }
}
