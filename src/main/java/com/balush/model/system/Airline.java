package com.balush.model.system;

import com.balush.model.object.ModelAH;
import com.balush.model.object.Airplane;
import com.balush.model.object.ModelBoeing;
import com.balush.model.object.ModelIL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Airline {
    private ArrayList<Flight> flightsList;
    private Map<Airplane, Boolean> airplaneList;

    /**
     * Add some flights and airplane just for course
     * should not be in the logic of the program
     */
    public Airline() {
        flightsList = new ArrayList<>();
        airplaneList = new HashMap<>();
        airplaneList.put(new ModelBoeing(), false);
        airplaneList.put(new ModelIL(), false);
        airplaneList.put(new ModelBoeing("Boeing-747", 65, 170, 120000,
                11000, 13, 130, 950), false);
        airplaneList.put(new ModelAH(), false);
    }

    public void showPassengersCapacity() {
        int passengersCapacity = 0;
        for (final Airplane i : airplaneList.keySet()) {
            passengersCapacity += i.getNumberOfPassengers();
        }
        for (final Flight i : flightsList) {
            passengersCapacity += i.getAirplane().getNumberOfPassengers();
        }
        System.out.println("Passengers capacity: " + passengersCapacity);
    }

    public void showLoadCapacity() {
        double loadCapacity = 0;
        for (final Airplane i : airplaneList.keySet()) {
            loadCapacity += i.getVolumeOfCargo();
        }
        for (final Flight i : flightsList) {
            loadCapacity += i.getAirplane().getVolumeOfCargo();
        }
        System.out.println("Load capacity: " + loadCapacity);
    }

    public void showPlaneSortByRange() {
        final ArrayList<Airplane> airplanes = new ArrayList<>(airplaneList.keySet());
        for (final Flight i : flightsList) {
            airplanes.add(i.getAirplane());
        }
        airplanes.sort(((o1, o2) -> (o1.getRangeOfFlight() > o2.getRangeOfFlight()) ? -1 : 0));
        for (final Airplane i : airplanes) {
            System.out.printf("%10s\t\t|%6d\n", i.getModel(), i.getRangeOfFlight());
        }
    }

    public void showPlaneByFuelConsumption() {
        final Scanner scanner = new Scanner(System.in);
        double lowerConsumption;
        double higherConsumption;
        System.out.println("Enter range of fuel consumption [lower : higher]: ");
        lowerConsumption = scanner.nextDouble();
        higherConsumption = scanner.nextDouble();
        System.out.println("Airplane with fuel consumption "
                + "higher than " + lowerConsumption + " and "
                + "lower than " + higherConsumption + ":");
        for (final Airplane i : airplaneList.keySet()) {
            if (i.getFuelConsumption() >= lowerConsumption
                    && i.getFuelConsumption() <= higherConsumption) {
                System.out.printf("%10s\t\t|%5d\n", i.getModel(), i.getFuelConsumption());
            }
        }
    }

    public void createAirplane() {
        final Scanner scanner = new Scanner(System.in);
        Airplane airplane;
        int modelAirplane = 0;
        System.out.println("1 - ModelBoeing" + '\n'
                + "2 - ModelIL" + '\n'
                + "3 - ModelAH");
        modelAirplane = scanner.nextInt();
        switch (modelAirplane) {
            case 1:
                airplane = new ModelBoeing();
                addAirplane(airplane);
                break;
            case 2:
                airplane = new ModelIL();
                addAirplane(airplane);
                break;
            case 3:
                airplane = new ModelAH();
                addAirplane(airplane);
                break;
            default:
        }
    }

    private void addAirplane(final Airplane airplane) {
        airplaneList.put(airplane, false);
    }

    public void chooseFlightForChange() {
        showAllFlights();
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of flight: ");
        final int idFlight = scanner.nextInt();
        for (final Flight i : flightsList) {
            if (idFlight == i.getFlightID()) {
                changeStatusFlight(i);
                break;
            }
        }
    }

    private void changeStatusFlight(final Flight flight) {
        for (final Flight i : flightsList) {
            if ((i.equals(flight)) && (i.getStatus())) {
                addAirplane(i.getAirplane());
                i.setStatus(!i.getStatus());
                deleteFlight(i);
                break;
            } else if (i.equals(flight)) {
                i.setStatus(!i.getStatus());
                break;
            }
        }
    }

    private void deleteFlight(final Flight flight) {
        flightsList.remove(flight);
    }

    public void createFlight() {
        Scanner scanner = new Scanner(System.in);
        Airplane airplane = null;
        for (final Airplane i : airplaneList.keySet()) {
            System.out.printf("%5d\t\t|%14s\t\t|%6b\n", i.getId(), i.getModel(), false);
        }
        System.out.println("Choose available id of airplane: ");
        final int availableID = scanner.nextInt();
        for (final Airplane i : airplaneList.keySet()) {
            if (availableID == i.getId()) {
                airplane = i;
                break;
            }
        }
        if (airplane != null) {
            scanner = new Scanner(System.in);
            System.out.println("Enter start city: ");
            final String cityFrom = scanner.nextLine();
            System.out.println("Enter end city: ");
            final String cityTo = scanner.nextLine();
            System.out.println("Enter departure date: ");
            final String departure = scanner.nextLine();
            System.out.println("Enter arrival date: ");
            final String arrival = scanner.nextLine();
            airplaneList.remove(airplane);
            addFlight(new Flight(airplane, cityFrom, cityTo, departure, arrival));
        }
    }

    private void addFlight(final Flight flight) {
        flightsList.add(flight);
    }

    public void showAllFlights() {
        for (final Flight i : flightsList) {
            i.showFlightInfo();
        }
    }

    public void showAllAirplanes() {
        for (final Flight i : flightsList) {
            System.out.printf("%5d\t\t|%14s\t\t|%6b\n", i.getAirplane().getId(), i.getAirplane().getModel(), i.getStatus());
        }
        for (final Airplane i : airplaneList.keySet()) {
            System.out.printf("%5d\t\t|%14s\t\t|%6b\n", i.getId(), i.getModel(), false);
        }
    }
}
