package com.balush.model.object;

public class ModelAH extends Airplane {
    public ModelAH() {
        model = "AH";
        length = 40.5;
        volumeOfCargo = 130.0;
        stockOfFuel = 100140;
        rangeOfFlight = 7600;
        fuelConsumption = 13;
        numberOfPassengers = 90;
        speed = 850;
        id = counterID++;
    }

    public ModelAH(final String model, final double length, final double volumeOfCargo,
                   final double stockOfFuel, final int rangeOfFlight, final int fuelConsumption,
                   final int numberOfPassengers, final int speed) {
        id = counterID++;
        setAll(model, length, volumeOfCargo, stockOfFuel, rangeOfFlight,
                fuelConsumption, numberOfPassengers, speed);
    }
}
