package com.balush.model.object;

public class ModelIL extends Airplane {
    public ModelIL() {
        model = "IL";
        length = 46.5;
        volumeOfCargo = 130.0;
        stockOfFuel = 101070;
        rangeOfFlight = 9100;
        fuelConsumption = 11;
        numberOfPassengers = 100;
        speed = 890;
        id = counterID++;
    }

    public ModelIL(final String model, final double length, final double volumeOfCargo,
                   final double stockOfFuel, final int rangeOfFlight, final int fuelConsumption,
                   final int numberOfPassengers, final int speed) {
        id = counterID++;
        setAll(model, length, volumeOfCargo, stockOfFuel, rangeOfFlight,
                fuelConsumption, numberOfPassengers, speed);
    }
}
