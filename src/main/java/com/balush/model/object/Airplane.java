package com.balush.model.object;

import java.util.Objects;

public abstract class Airplane {
    protected static int counterID;
    protected String model;
    protected double length;
    protected double volumeOfCargo;
    protected double stockOfFuel;
    protected int rangeOfFlight;
    protected int fuelConsumption;
    protected int numberOfPassengers;
    protected int speed;
    protected int id;

    public Airplane() {}

    public String getModel() {
        return model;
    }

    public void setModel(final String model) {
        this.model = model;
    }

    public double getLength() {
        return length;
    }

    public void setLength(final double length) {
        this.length = length;
    }

    public double getVolumeOfCargo() {
        return volumeOfCargo;
    }

    public void setVolumeOfCargo(final double volumeOfCargo) {
        this.volumeOfCargo = volumeOfCargo;
    }

    public double getStockOfFuel() {
        return stockOfFuel;
    }

    public void setStockOfFuel(final double stockOfFuel) {
        this.stockOfFuel = stockOfFuel;
    }

    public int getRangeOfFlight() {
        return rangeOfFlight;
    }

    public void setRangeOfFlight(final int rangeOfFlight) {
        this.rangeOfFlight = rangeOfFlight;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(final int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(final int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(final int speed) {
        this.speed = speed;
    }

    public int getId() {
        return id;
    }

    public void setAll(final String model, final double length, final double volumeOfCargo,
                       final double stockOfFuel, final int rangeOfFlight, final int fuelConsumption,
                       final int numberOfPassengers, final int speed) {
        this.model = model;
        this.length = length;
        this.volumeOfCargo = volumeOfCargo;
        this.stockOfFuel = stockOfFuel;
        this.rangeOfFlight = rangeOfFlight;
        this.fuelConsumption = fuelConsumption;
        this.numberOfPassengers = numberOfPassengers;
        this.speed = speed;
    }

    public void showData(){
        System.out.println("model: " + model + '\n'
                + "Length: " + length + '\n'
                + "Volume of cargo: " + volumeOfCargo + '\n'
                + "Stock of fuel: " + stockOfFuel + '\n'
                + "Range of flight: " + rangeOfFlight + '\n'
                + "Fuel consumption: " + fuelConsumption + '\n'
                + "Number of passengers: " + numberOfPassengers + '\n'
                + "Speed: " + speed + '\n'
                + "id: " + id + '\n'
                + "_________________________________");
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Airplane airplane = (Airplane) o;
        return Double.compare(airplane.length, length) == 0 &&
                Double.compare(airplane.volumeOfCargo, volumeOfCargo) == 0 &&
                Double.compare(airplane.stockOfFuel, stockOfFuel) == 0 &&
                rangeOfFlight == airplane.rangeOfFlight &&
                fuelConsumption == airplane.fuelConsumption &&
                numberOfPassengers == airplane.numberOfPassengers &&
                speed == airplane.speed &&
                id == airplane.id &&
                Objects.equals(model, airplane.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, length, volumeOfCargo, stockOfFuel,
                rangeOfFlight, fuelConsumption, numberOfPassengers, speed, id);
    }
}
