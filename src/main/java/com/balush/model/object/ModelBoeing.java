package com.balush.model.object;

public class ModelBoeing extends Airplane {
    public ModelBoeing() {
        model = "Boeing";
        length = 63.7;
        volumeOfCargo = 150.9;
        stockOfFuel = 117340;
        rangeOfFlight = 9700;
        fuelConsumption = 12;
        numberOfPassengers = 110;
        speed = 917;
        id = counterID++;
    }

    public ModelBoeing(final String model, final double length, final double volumeOfCargo,
                       final double stockOfFuel, final int rangeOfFlight, final int fuelConsumption,
                       final int numberOfPassengers, final int speed) {
        id = counterID++;
        setAll(model, length, volumeOfCargo, stockOfFuel, rangeOfFlight,
                fuelConsumption, numberOfPassengers, speed);
    }
}
