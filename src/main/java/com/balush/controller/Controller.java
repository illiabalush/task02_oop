package com.balush.controller;

import com.balush.model.system.Airline;
import com.balush.view.View;

import java.util.Scanner;

public class Controller {
    private Airline airline;
    private View view;
    private Scanner scanner;

    public Controller() {
        airline = new Airline();
        view = new View();
        scanner = new Scanner(System.in);
    }

    private void userController() {
        int userFunction;
        while (true) {
            view.showUserFunction();
            userFunction = scanner.nextInt();
            switch (userFunction) {
                case 1:
                    airline.showPassengersCapacity();
                    break;
                case 2:
                    airline.showLoadCapacity();
                    break;
                case 3:
                    airline.showPlaneSortByRange();
                    break;
                case 4:
                    airline.showPlaneByFuelConsumption();
                    break;
                default:
                    return;
            }
        }
    }

    private void adminController() {
        int userFunction;
        while (true) {
            view.showAdminFunction();
            userFunction = scanner.nextInt();
            switch (userFunction) {
                case 1:
                    airline.showPassengersCapacity();
                    break;
                case 2:
                    airline.showLoadCapacity();
                    break;
                case 3:
                    airline.showPlaneSortByRange();
                    break;
                case 4:
                    airline.showPlaneByFuelConsumption();
                    break;
                case 5:
                    airline.createAirplane();
                    break;
                case 6:
                    airline.chooseFlightForChange();
                    break;
                case 7:
                    airline.showAllFlights();
                    break;
                case 8:
                    airline.showAllAirplanes();
                    break;
                case 9:
                    airline.createFlight();
                    break;
                default:
                    return;
            }
        }
    }

    public void start() {
        int viewMode;
        while (true) {
            view.showStartMenu();
            viewMode = scanner.nextInt();
            switch (viewMode) {
                case 1:
                    adminController();
                    break;
                case 2:
                    userController();
                    break;
                default:
                    System.out.println("Exit...");
                    return;
            }
        }
    }
}
