package com.balush.application;

import com.balush.controller.Controller;

public class Main {
    public static void main(final String[] args) {
        final Controller controller = new Controller();
        controller.start();
    }
}

